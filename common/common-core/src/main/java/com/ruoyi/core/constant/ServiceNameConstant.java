package com.ruoyi.core.constant;

/**
 * 服务名称
 * @author wxh
 * @date 2024/5/15 0015
 */
public class ServiceNameConstant {

    /**
     * 系统模块的serviceId
     */
    public static final  String SYSTEM_SERVICE = "system";
}
