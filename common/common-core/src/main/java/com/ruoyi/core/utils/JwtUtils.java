package com.ruoyi.core.utils;

import com.ruoyi.core.constant.SecurityConstants;
import com.ruoyi.core.constant.TokenConstants;
import com.ruoyi.core.text.Convert;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Map;


/**
 * Jwt工具类
 * @author wxh
 * @date 2024/5/14 0014
 */
public class JwtUtils {

    public static String secret = TokenConstants.SECRET;

    /**
     * 生成token
     * @param claims
     * @return
     */
    public static String createToken(Map<String,Object> claims){
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
        return  token;
    }



    /**
     * 从令牌中获取数据声明
     * @param token
     * @return
     */
    public  static Claims parseToken(String token){
        return Jwts.parser().setSigningKey(secret).parseClaimsJwt(token).getBody();
    }


    /**
     *  根据令牌获取用户标识
     * @return
     */
    public static  String  getUserKey(String token){
        Claims claims = parseToken(token);
        return getValue(claims,SecurityConstants.USER_KEY);
    }

    /**
     * 从令牌中获取用户名
     * @param token
     * @return
     */
    public  static String getUsername(String token){
        Claims claims = parseToken(token);
        return getValue(claims, SecurityConstants.DETAILS_USERNAME);
    }

    /**
     * 根据身份信息获取键值
     * @param claims
     * @param key
     * @return
     */
    public static String getValue(Claims claims,String key){
        return Convert.toStr(claims.get(key),"");
    }

}
