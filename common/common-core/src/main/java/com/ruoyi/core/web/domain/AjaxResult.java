package com.ruoyi.core.web.domain;

import com.ruoyi.core.constant.HttpStatus;
import com.ruoyi.core.utils.StringUtils;
import com.sun.corba.se.spi.ior.ObjectKey;

import java.util.HashMap;
import java.util.Objects;

/**
 * 操作消息提醒
 *
 * @author wxh
 * @date 2024/5/7 0007
 */
public class AjaxResult extends HashMap<String, Object> {

    /**
     * 状态码
     */
    private static final String CODE_TAG = "code";

    /**
     * 数据对象
     */
    private static final String DATE_TAG = "data";

    /**
     * 返回内容
     */
    private static final String MSG_TAG = "msg";

    public AjaxResult() {
    }

    public AjaxResult(int code, String msg) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    public AjaxResult(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (StringUtils.isNotNull(data)) {
            super.put(DATE_TAG, data);
        }
    }

    /**
     * 返回成功消息
     *
     * @return
     */
    public static AjaxResult success() {
        return AjaxResult.success("操作成功");
    }

    /**
     * 返回成功消息
     *
     * @param data
     * @return
     */
    public static AjaxResult success(Object data) {
        return AjaxResult.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg
     * @return
     */
    public static AjaxResult success(String msg) {
        return AjaxResult.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg
     * @param data
     * @return
     */
    public static AjaxResult success(String msg, Object data) {
        return new AjaxResult(HttpStatus.SUCCESS, msg, data);
    }


    /**
     * 返回告警消息
     *
     * @param msg
     * @return
     */
    public static AjaxResult warn(String msg) {
        return AjaxResult.warn(msg, null);
    }

    /**
     * 返回告警消息
     *
     * @param msg
     * @param data
     * @return
     */
    public static AjaxResult warn(String msg, Object data) {
        return new AjaxResult(HttpStatus.WARN, msg, data);
    }

    /**
     * 返回错误消息
     * @return
     */
    public static  AjaxResult error(){
        return  AjaxResult.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg
     * @return
     */
    public static AjaxResult error(String msg) {
        return AjaxResult.error(msg, null);
    }

    /**
     * 返回错误消息
     * @param code
     * @param msg
     * @return
     */
    public static AjaxResult error(int code, String msg) {
        return new AjaxResult(code, msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg
     * @param data
     * @return
     */
    public static AjaxResult error(String msg, Object data) {
        return new AjaxResult(HttpStatus.ERROR, msg, data);
    }

    /**
     * 是否为成功消息
     * @return
     */
    public boolean isSuccess(){
        return Objects.equals(HttpStatus.SUCCESS,this.get(CODE_TAG));
    }

    /**
     * 是否为警告消息
     * @return
     */
    public boolean isWarn(){
        return Objects.equals(HttpStatus.WARN,this.get(CODE_TAG));
    }

    /**
     * 是否为错误消息
     * @return
     */
    public boolean isError(){
        return Objects.equals(HttpStatus.ERROR,this.get(CODE_TAG));
    }


}
