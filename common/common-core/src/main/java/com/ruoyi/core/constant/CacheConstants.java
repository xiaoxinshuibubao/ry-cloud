package com.ruoyi.core.constant;

/**
 * 缓存常量信息
 * @author wxh
 * @date 2024/5/10 0010
 */
public class CacheConstants {

    /**
     * 验证码 redis key
     */
    public static  final  String CAPTCHA_CODE_KEY = "captcha_code";

    /**
     * 用户名密码登录最大尝试次数
     */
    public static  final  int PASSWORD_MAX_RETRY_COUNT = 5;

    /**
     * 密码锁定时间
     */
    public static final Long PASSWORD_LOCK_TIME = 10L;

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";
    /**
     * 登录IP黑名单 cache key
     */
    public static final String SYS_LOGIN_BLACKIPLIST = SYS_CONFIG_KEY + "sys.login.blackIPList";

    /**
     * 登录账户密码错误次数 redis key
     */
    public static final String PWD_ERR_CNT_KEY = "pwd_err_cnt:";

    /**
     * 缓存有效期，默认720分钟
     */
    public static final long EXPIRATION = 720;

    /**
     * 权限缓存前缀
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens";
}
