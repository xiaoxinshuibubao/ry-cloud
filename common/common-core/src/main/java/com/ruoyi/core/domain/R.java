package com.ruoyi.core.domain;

import com.ruoyi.core.constant.Constants;

import java.io.Serializable;

/**
 * 响应消息主题
 * @author wxh
 * @date 2024/5/13 0013
 */
public class R<T> implements Serializable {

    public static  final  int SUCCESS = Constants.SUCCESS;
    public static  final  int FAIL = Constants.FAIL;

    private int code;
    private String message;
    private T data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> R<T> ok(){
        return restResult(SUCCESS,null,null);
    }

    public static <T> R<T> ok(T data){
        return restResult(SUCCESS,data,null);
    }

    public static <T> R<T> ok(T data,String message){
        return restResult(SUCCESS,data,message);
    }

    public static <T> R<T> fail(int code, String  message){
        return restResult(code,null,message);
    }

    public static <T> R<T> fail(String message){
        return restResult(FAIL,null,message);
    }
    public  static <T> R<T> restResult(int code,T data,String message){
        R<T> apiResult = new R<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMessage(message);
        return apiResult;
    }
}
