package com.ruoyi.core.exception;

/**
 * @author wxh
 * @date 2024/5/9 0009
 */
public class CaptchaException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public CaptchaException(String msg){
        super(msg);
    }
}
