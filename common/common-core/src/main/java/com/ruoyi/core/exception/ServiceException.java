package com.ruoyi.core.exception;

/**
 * @author wxh
 * @date 2024/5/13 0013
 */
public class ServiceException extends RuntimeException{
    /**
     * 错误码
     */
     private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误明细，内部调试错误
     */
    private String detailMessage;

    public ServiceException() {

    }

    public ServiceException(String message) {
        this.message = message;
    }

    public ServiceException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    public Integer getCode() {
        return code;
    }

    public ServiceException  setMessage(String message){
        this.message = message;
        return this;
    }

    public ServiceException setDetailMessage(String detailMessage){
        this.detailMessage = detailMessage;
        return this;
    }
}
