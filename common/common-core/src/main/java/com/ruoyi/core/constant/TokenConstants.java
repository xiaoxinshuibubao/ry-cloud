package com.ruoyi.core.constant;

/**
 * Token的Key常量
 * @author wxh
 * @date 2024/5/15 0015
 */
public class TokenConstants {

    /**
     * 令牌自定义标识
     */
    public  static  final  String  AUTHORIZATION = "Authorization";

    /**
     * 令牌前缀
     */
    public  static  final  String  PREFIX = "Bearer";
    /**
     * 令牌密钥
     */
    public  static  final  String  SECRET = "abcdefghijklmnopqrstuvwxyz";
}
