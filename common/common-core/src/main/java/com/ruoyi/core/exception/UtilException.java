package com.ruoyi.core.exception;

/**
 * 工具类异常
 * @author wxh
 * @date 2024/5/10 0010
 */
public class UtilException extends RuntimeException {

    public UtilException(Throwable e) {
        super(e.getMessage());
    }

    public UtilException(String message) {
        super(message);
    }

    public UtilException(String message, Throwable e) {
        super(message, e);
    }
}
