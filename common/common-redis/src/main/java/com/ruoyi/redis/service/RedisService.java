package com.ruoyi.redis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * @author wxh
 * @date 2024/5/10 0010
 */
@Component
public class RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 缓存基本的对象
     * @param key 键
     * @param value 值
     * @param <T>
     */
    public <T> void setCacheObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 缓存基本的对象
     * @param key 键
     * @param value 值
     * @param timeout 时间
     * @param timeUnit 时间颗粒度
     * @param <T>
     */
    public <T> void setCacheObject(final String key, final  T value, final  Long timeout, final TimeUnit timeUnit){
        redisTemplate.opsForValue().set(key,value,timeout,timeUnit);
    }

    /**
     * 获得缓存的基本对象
     * @param key 键
     * @return
     * @param <T>
     */
    public  <T> T getCacheObject(final String key){
        ValueOperations<String,T> valueOperations = redisTemplate.opsForValue();
        return valueOperations.get(key);
    }

    /**
     *  判断key值是否存在
     * @param key 键
     * @return
     */
    public Boolean hasKey(String key){
        return redisTemplate.hasKey(key);
    }

    /**
     * 删除单个对象
     * @param key 键
     * @return
     */
    public boolean deleteObject(final String key){
        return redisTemplate.delete(key);
    }


}
