package com.ruoyi.common.security.service;

import com.ruoyi.common.security.auth.AuthUtil;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.core.constant.CacheConstants;
import com.ruoyi.core.constant.SecurityConstants;
import com.ruoyi.core.utils.JwtUtils;
import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.core.utils.ip.IpUtils;
import com.ruoyi.core.utils.uuid.IdUtils;
import com.ruoyi.redis.service.RedisService;
import com.ruoyi.system.api.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.est.CACertsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Token验证处理
 * @author wxh
 * @date 2024/5/14 0014
 */
@Component
@Slf4j
public class TokenService {

    protected  static final long  MILLIS_SECOND = 1000;

    protected  static final long MILLIS_MINUTE = 60 * MILLIS_SECOND;

    public static Long expireTime = CacheConstants.EXPIRATION;

    public  static String ACCESS_TOKEN = CacheConstants.LOGIN_TOKEN_KEY;

    @Autowired
    private RedisService redisService;

    /**
     * 创建令牌
     * @param loginUser
     * @return
     */
    public Map<String,Object> createToken(LoginUser loginUser){

        String token = IdUtils.fastUUID();
        Long userId = loginUser.getSysUser().getUserId();
        String username = loginUser.getSysUser().getUserName();
        loginUser.setToken(token);
        loginUser.setUserId(userId);
        loginUser.setUsername(username);
        loginUser.setIpaddr(IpUtils.getIpAddr());
        refreshToken(loginUser);

        // jwt存储信息
        HashMap<String, Object> claims = new HashMap<>();
        claims.put(SecurityConstants.USER_KEY,token);
        claims.put(SecurityConstants.DETAILS_USER_ID,userId);
        claims.put(SecurityConstants.DETAILS_USERNAME,username);

        // 接口返回信息
        HashMap<String, Object> rspMap = new HashMap<>();
        rspMap.put("access_token", JwtUtils.createToken(claims));
        rspMap.put("expires_in",expireTime);
        return rspMap;
    }

    public LoginUser getLoginUser(HttpServletRequest request){
        String token = SecurityUtils.getToken(request);
        return  getLoginUser(token);
    }

    public LoginUser getLoginUser(String token){
        LoginUser user = null;
        try {
            if (StringUtils.isNotEmpty(token)){
                String userKey = JwtUtils.getUserKey(token);
                user = redisService.getCacheObject(getCacheKey(userKey));
                return user;
            }
        }catch (Exception e){
            log.info("获取用户信息异常,{}",e.getMessage());
        }
        return user;
    }

    /**
     * 刷新令牌
     * @param loginUser
     */
    public  void refreshToken(LoginUser loginUser){
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(System.currentTimeMillis()+ expireTime * MILLIS_MINUTE);

        // 根据uuid生成key值
        String userKey = getCacheKey(loginUser.getToken());
        redisService.setCacheObject(userKey,loginUser,expireTime, TimeUnit.MINUTES);
    }

    /**
     * 删除用户缓存信息
     * @param token
     */
    public void delLoginUser(String token){
        if (StringUtils.isNotEmpty(token)){
            String userKey = JwtUtils.getUserKey(token);
            redisService.deleteObject(getCacheKey(userKey));
        }
    }

    public String getCacheKey(String token){
        return ACCESS_TOKEN + token;
    }
}
