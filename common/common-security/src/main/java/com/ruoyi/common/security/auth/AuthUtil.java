package com.ruoyi.common.security.auth;

/**
 * token 权限验证工具类
 * @author wxh
 * @date 2024/5/24 0024
 */
public class AuthUtil {

    public static AuthLogic authLogic = new AuthLogic();

    /**
     * 注销会话, 根据指定token
     * @param token
     */
    public static void logoutByToken(String token){
        authLogic.logoutByToken(token);
    }
}
