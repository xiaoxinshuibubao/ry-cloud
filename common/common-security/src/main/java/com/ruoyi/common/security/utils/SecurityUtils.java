package com.ruoyi.common.security.utils;

import com.ruoyi.core.constant.TokenConstants;
import com.ruoyi.core.utils.ServletUtils;
import com.ruoyi.core.utils.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;

/**
 * 权限获取工具类
 * @author wxh
 * @date 2024/5/14 0014
 */
public class SecurityUtils {


    /**
     * 获取请求token
     * @return
     */
    public static String getToken(){
        return getToken(ServletUtils.getRequest());
    }

    /**
     * 根据request获取token
     * @param request
     * @return
     */
    public static String getToken(HttpServletRequest request){
        String token = request.getHeader(TokenConstants.AUTHORIZATION);
        return replaceTokenPrefix(token);
    }

    /**
     * 裁剪令牌前缀, 前端
     * @param token
     * @return
     */
     public  static  String replaceTokenPrefix(String token){
        if (StringUtils.isNotEmpty(token) && token.startsWith(TokenConstants.PREFIX)){
            token = token.replaceFirst(TokenConstants.PREFIX,"");
        }
        return token;
     }

    /**
     * 生成bCryptPasswordEncoder密码
     * @param password
     * @return 加密字符串
     */
     public static String encryptPassword(String password){
         BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
         return bCryptPasswordEncoder.encode(password);
     }


    /**
     * 判断密码是否相同
     * @param rawPassword
     * @param encodedPassword
     * @return
     */
    public static boolean matchesPassword(String rawPassword,String encodedPassword){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.matches(rawPassword,encodedPassword);
    }
}
