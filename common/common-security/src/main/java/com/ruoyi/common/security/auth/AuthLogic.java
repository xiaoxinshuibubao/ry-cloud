package com.ruoyi.common.security.auth;

import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.core.utils.SpringUtils;

/**
 * 权限验证，逻辑实现类
 * @author wxh
 * @date 2024/5/24 0024
 */
public class AuthLogic {

    public TokenService tokenService = SpringUtils.getBean(TokenService.class);

    /**
     * 会话注销，根据指定token
     * @param token
     */
    public void logoutByToken(String token){
        tokenService.delLoginUser(token);
    }
}
