package com.ruoyi.system.service;

/**
 * 系统配置 服务层
 * @author wxh
 * @date 2024/5/27 0027
 */
public interface ISysConfigService {
    public String selectConfigByKey(String configKey);
}
