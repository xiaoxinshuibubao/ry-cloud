package com.ruoyi.system.service;

import com.ruoyi.system.api.domain.SysUser;

/**
 * @author wxh
 * @date 2024/5/21 0021
 */
public interface ISysUserService {

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    public SysUser selectSysUserByUserName(String username);

    /**
     * 校验用户名是否唯一
     * @param user
     * @return
     */
    public boolean checkUserNameUnique(SysUser user);

    /**
     * 注册用户信息
     * @param user
     * @return
     */
    public boolean register(SysUser user);
}
