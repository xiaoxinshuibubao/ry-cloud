package com.ruoyi.system.service.impl;

import com.ruoyi.system.api.domain.SysLogininfor;
import com.ruoyi.system.mapper.SysLogininforMapper;
import com.ruoyi.system.service.ISysLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统访问日志情况信息 服务层处理
 * @author wxh
 * @date 2024/5/21 0021
 */
@Service
public class SysLogininforServiceImpl implements ISysLogininforService {

    @Autowired
    private SysLogininforMapper sysLogininforMapper;

    /**
     * 新增系统登录日志
     * @param sysLogininfor 访问日志对象
     * @return
     */
    @Override
    public int insertLogininfor(SysLogininfor sysLogininfor) {
        return sysLogininforMapper.insertLogininfor(sysLogininfor);
    }
}
