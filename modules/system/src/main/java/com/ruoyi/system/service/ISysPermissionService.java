package com.ruoyi.system.service;

import com.ruoyi.system.api.domain.SysUser;

import java.util.Set;

/**
 *  权限信息 服务层
 * @author wxh
 * @date 2024/5/21 0021
 */
public interface ISysPermissionService {

    /**
     * 获取角色数据权限
     * @param user
     * @return
     */
    Set<String> getRolePermission(SysUser user);

    /**
     * 获取菜单数据权限
     * @param user
     * @return
     */
    Set<String> getMenuPermission(SysUser user);
}
