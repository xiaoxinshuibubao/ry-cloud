package com.ruoyi.system.domain;

import com.ruoyi.core.web.domain.BaseEntity;

import java.util.Date;

/**
 *  参数配置表 sys_config
 * @author wxh
 * @date 2024/5/27 0027
 */

public class SysConfig extends BaseEntity {
    private int configId;
    /**
     * 参数名称
     */
    private String configName;
    /**
     * 参数键名
     */
    private String configKey;
    /**
     * 参数键值
     */
    private String configValue;

    /**
     * 是否内置（Y是 N否）
     */
    private String ConfigType;

    public int getConfigId() {
        return configId;
    }

    public void setConfigId(int configId) {
        this.configId = configId;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public String getConfigType() {
        return ConfigType;
    }

    public void setConfigType(String configType) {
        ConfigType = configType;
    }
}
