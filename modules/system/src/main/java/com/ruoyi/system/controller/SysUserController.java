package com.ruoyi.system.controller;

import com.ruoyi.core.domain.R;
import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysPermissionService;
import com.ruoyi.system.service.ISysRoleService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * 用户信息
 * @author wxh
 * @date 2024/5/21 0021
 */
@RestController
@RequestMapping("user")
public class SysUserController {

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISysPermissionService iSysPermissionService;

    @Autowired
    private ISysConfigService iSysConfigService;



    @GetMapping("/info/{username}")
    public R<LoginUser> info(@PathVariable("username") String username){

        SysUser sysUser = iSysUserService.selectSysUserByUserName(username);
        if (StringUtils.isNull(sysUser)){
            return R.fail("用户名错误");
        }
        // 角色集合
        Set<String> rolePermission = iSysPermissionService.getRolePermission(sysUser);
        // 权限集合
        Set<String> menuPermission = iSysPermissionService.getMenuPermission(sysUser);
        LoginUser sysUserVo = new LoginUser();
        sysUserVo.setRoles(rolePermission);
        sysUserVo.setPermissions(menuPermission);
        sysUserVo.setSysUser(sysUser);
        return R.ok(sysUserVo);
    }

    /**
     * 用户注册
     * @param sysUser
     * @return
     */
    @PostMapping("/register")
    public R<Boolean> register(@RequestBody SysUser sysUser){
        // 是否开启注册功能
        String userName = sysUser.getUserName();
        if (!"true".equals(iSysConfigService.selectConfigByKey("sys.account.registerUser"))){
            return R.fail("当前系统没有开启注册功能！");
        }
        // 判断用户是否存在
        if (!iSysUserService.checkUserNameUnique(sysUser)){
            return R.fail("报存用户"+userName+"失败，注册账号已存在");
        }
        // 保存用户信息
        return  R.ok(iSysUserService.register(sysUser));
    }
}
