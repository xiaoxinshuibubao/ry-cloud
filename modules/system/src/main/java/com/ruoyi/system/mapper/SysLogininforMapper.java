package com.ruoyi.system.mapper;

import com.ruoyi.system.api.domain.SysLogininfor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wxh
 * @date 2024/5/21 0021
 */
@Mapper
public interface SysLogininforMapper {

    public int insertLogininfor(SysLogininfor logininfor);
}
