package com.ruoyi.system.mapper;

import com.ruoyi.system.api.domain.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author wxh
 * @date 2024/5/22 0022
 */
@Mapper
public interface SysRoleMapper {

    /**
     * 根据userId查询角色信息
     * @param id 用户ID
     * @return
     */
    List<SysRole> selectRolePermissionByUserId(Long id);
}
