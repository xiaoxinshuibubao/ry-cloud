package com.ruoyi.system.service;

import com.ruoyi.system.api.domain.SysLogininfor;

/**
 * 系统访问日志情况信息 服务层
 * @author wxh
 * @date 2024/5/21 0021
 */
public interface ISysLogininforService {

    /**
     *  新增系统登录日志
     * @param sysLogininfor 访问日志对象
     * @return
     */
    public  int insertLogininfor(SysLogininfor sysLogininfor);
}
