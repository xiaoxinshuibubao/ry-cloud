package com.ruoyi.system.service;

import java.util.Set;

/**
 * 菜单权限 Service类
 * @author wxh
 * @date 2024/5/23 0023
 */
public interface ISysMenuService {

    /**
     * 根据角色ID查询权限信息
     * @param roleId
     * @return
     */
    Set<String> selectMenuPermissionByRoleId(Long roleId);

    Set<String> selectMenuPermissionByUserId(Long userId);
}
