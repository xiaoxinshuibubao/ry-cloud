package com.ruoyi.system.service.impl;

import com.ruoyi.core.constant.UserConstants;
import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wxh
 * @date 2024/5/21 0021
 */
@Service
public class SysUserServiceImpl implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 根据用户名查询用户
     *
     * @param username
     * @return
     */
    @Override
    public SysUser selectSysUserByUserName(String username) {
        return sysUserMapper.selectSysUserByUserName(username);
    }

    /**
     * 校验用户名是否唯一
     *
     * @param user
     * @return
     */
    @Override
    public boolean checkUserNameUnique(SysUser user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        // 查询数据库
        SysUser info = sysUserMapper.checkUserNameUnique(user.getUserName());
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) { // 已经存在该账号了
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 注册用户信息
     *
     * @param user
     * @return
     */
    @Override
    public boolean register(SysUser user) {
        return sysUserMapper.insertUser(user) > 0;
    }


}
