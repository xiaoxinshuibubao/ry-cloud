package com.ruoyi.system.service;

import java.util.Set;

/**
 * 角色
 * @author wxh
 * @date 2024/5/21 0021
 */
public interface ISysRoleService {
    /**
     * 根据userId查询权限信息
     * @param userId
     * @return
     */
    Set<String> selectRolePermissionByUserId(Long userId);
}
