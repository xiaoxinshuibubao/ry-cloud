package com.ruoyi.system.service.impl;

import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.system.mapper.SysMenuMapper;
import com.ruoyi.system.mapper.SysRoleMapper;
import com.ruoyi.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author wxh
 * @date 2024/5/23 0023
 */
@Service
public class SysMenuServiceImpl implements ISysMenuService {
    
    @Autowired
    private SysMenuMapper sysMenuMapper;
    
    /**
     * 根据角色ID查询权限
     * @param roleId
     * @return
     */
    @Override
    public Set<String> selectMenuPermissionByRoleId(Long roleId) {
        List<String> perms = sysMenuMapper.selectMenuPermissionByRoleId(roleId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms) {
            if (StringUtils.isNotNull(perm)){
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据用户ID查询权限
     * @param userId
     * @return
     */
    @Override
    public Set<String> selectMenuPermissionByUserId(Long userId) {

        return null;
    }
}
