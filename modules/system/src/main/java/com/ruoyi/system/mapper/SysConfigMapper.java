package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 参数配置  数据层
 * @author wxh
 * @date 2024/5/27 0027
 */
@Mapper
public interface SysConfigMapper {

    public SysConfig selectConfig(SysConfig sysConfig);
}
