package com.ruoyi.system.service.impl;

import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.service.ISysMenuService;
import com.ruoyi.system.service.ISysPermissionService;
import com.ruoyi.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 权限信息
 * @author wxh
 * @date 2024/5/21 0021
 */
@Service
public class SysPermissionServiceImpl implements ISysPermissionService {

    @Autowired
    private ISysRoleService iSysRoleService;

    @Autowired
    private ISysMenuService iSysMenuService;
    
    /**
     * 获取角色数据权限
     * @param user
     * @return
     */
    @Override
    public Set<String> getRolePermission(SysUser user) {

        HashSet<String> roles = new HashSet<>();
        if (user.isAdmin(user.getUserId())){ // 判断是否为管理员
            roles.add("admin");
        }else { // 不是管理员，查询其对应权限
            roles.addAll(iSysRoleService.selectRolePermissionByUserId(user.getUserId()));
        }
        return roles;
    }

    /**
     * 获取菜单数据权限
     * @param user
     * @return
     */
    @Override
    public Set<String> getMenuPermission(SysUser user) {

        Set<String> perms = new HashSet<>();
        // 管理员拥有所有权限
        if (user.isAdmin()){
            perms.add("*.*.*");
        }else{
            List<SysRole> roles = user.getRoles();
            if (!CollectionUtils.isEmpty(roles)){
                for (SysRole role : roles) {
                    Set<String> permsSet = iSysMenuService.selectMenuPermissionByRoleId(role.getRoleId());
                    role.setPermissions(permsSet);
                    perms.addAll(permsSet);
                }
            }else {
                perms.addAll(iSysMenuService.selectMenuPermissionByUserId(user.getUserId()));
            }
        }
        return perms;
    }
}
