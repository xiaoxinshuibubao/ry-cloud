package com.ruoyi.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author wxh
 * @date 2024/5/23 0023
 */
@Mapper
public interface SysMenuMapper {

    List<String> selectMenuPermissionByRoleId(Long roleId);


    List<String> selectMenuPermissionByUserId(Long userId);
}
