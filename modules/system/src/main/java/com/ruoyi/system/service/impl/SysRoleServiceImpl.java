package com.ruoyi.system.service.impl;

import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.mapper.SysRoleMapper;
import com.ruoyi.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author wxh
 * @date 2024/5/22 0022
 */

@Service
public class SysRoleServiceImpl implements ISysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     * 根据用户Id查询权限
     * @param userId
     * @return
     */
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        Set<String> set = new HashSet<>();
        List<SysRole> sysRoles = sysRoleMapper.selectRolePermissionByUserId(userId);
        for (SysRole sysRole : sysRoles) {
            if (StringUtils.isNotNull(sysRole)){
                set.addAll(Arrays.asList(sysRole.getRoleKey().trim().split(",")));
            }
        }
        return set;
    }
}
