package com.ruoyi.system.service.impl;

import com.ruoyi.core.constant.CacheConstants;
import com.ruoyi.core.text.Convert;
import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.redis.service.RedisService;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.mapper.SysConfigMapper;
import com.ruoyi.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统配置服务层 实现类
 * @author wxh
 * @date 2024/5/27 0027
 */
@Service
public class SysConfigServiceImpl implements ISysConfigService {

    @Autowired
    private RedisService redisService;

    @Autowired
    private SysConfigMapper sysConfigMapper;

    /**
     * 根据键名查询参数配置信息
     * @param configKey
     * @return
     */
    @Override
    public String selectConfigByKey(String configKey) {
        // 先查redis
        String configValue = Convert.toStr(redisService.getCacheObject(getCacheKey(configKey)));
        if (StringUtils.isNotEmpty(configValue)){
            return configValue;
        }
        // 没有再查库， 并重新生成redis
        SysConfig sysConfig = new SysConfig();
        sysConfig.setConfigKey(configKey);
        SysConfig retConfig = sysConfigMapper.selectConfig(sysConfig);
        if (StringUtils.isNotNull(retConfig)){
            redisService.setCacheObject(getCacheKey(configKey),retConfig.getConfigValue());
            return retConfig.getConfigValue();
        }
        return StringUtils.EMPTY;
    }

    public String getCacheKey(String key){
        return CacheConstants.SYS_CONFIG_KEY;
    }
}
