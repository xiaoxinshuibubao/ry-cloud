package com.ruoyi.system.mapper;

import com.ruoyi.system.api.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wxh
 * @date 2024/5/21 0021
 */
@Mapper
public interface SysUserMapper {

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return
     */
    SysUser selectSysUserByUserName(String username);

    SysUser checkUserNameUnique(String username);

    int insertUser(SysUser user);
}
