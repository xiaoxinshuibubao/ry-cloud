package com.ruoyi.system.controller;

import com.ruoyi.core.web.controller.BaseController;
import com.ruoyi.core.web.domain.AjaxResult;
import com.ruoyi.system.api.domain.SysLogininfor;
import com.ruoyi.system.service.ISysLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  系统访问记录
 * @author wxh
 * @date 2024/5/21 0021
 */
@RestController
@RequestMapping("/logininfor")
public class SysLogininforController extends BaseController {

    @Autowired
    private ISysLogininforService ISysLogininforService;

    @PostMapping()
    public AjaxResult add(@RequestBody SysLogininfor sysLogininfor){
        return toAjax(ISysLogininforService.insertLogininfor(sysLogininfor));
    }
}
