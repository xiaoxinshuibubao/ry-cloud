package com.ruoyi.auth.form;

/**
 * 用户登录对象
 * @author wxh
 * @date 2024/5/14 0014
 */
public class LoginBody {
    /**
     * 用户名
     */
    private  String username;

    /**
     * 密码
     */
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
