package com.ruoyi.auth.controller;

import com.ruoyi.auth.form.LoginBody;
import com.ruoyi.auth.form.RegisterBody;
import com.ruoyi.auth.service.SysLoginService;
import com.ruoyi.auth.service.SysRecordLogService;
import com.ruoyi.common.security.auth.AuthUtil;
import com.ruoyi.common.security.service.TokenService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.core.constant.Constants;
import com.ruoyi.core.domain.R;
import com.ruoyi.core.utils.JwtUtils;
import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.system.api.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wxh
 * @date 2024/5/11 0011
 */
@RestController
@Slf4j
public class TokenController {
    @Autowired
    private SysLoginService sysLoginService;

    @Autowired
    private TokenService tokenService;


    /**
     * 登录接口
     * @param form
     * @return
     */
    @PostMapping("/login")
    public R<?> login(@RequestBody LoginBody form){
        log.info("用户开始登录:{}",form);
        //用户登录
        LoginUser userLogin = sysLoginService.login(form.getUsername(), form.getPassword());
        return  R.ok(tokenService.createToken(userLogin));
    }


    /**
     * 退出接口
     * @param request
     * @return
     */
    @DeleteMapping("/logout")
    public R<?> logout(HttpServletRequest request){
        // 清空用户缓存
        String token = SecurityUtils.getToken(request);
        if (StringUtils.isNotEmpty(token)){
            String username = JwtUtils.getUsername(token);
            AuthUtil.logoutByToken(token);
            // 记录退出登录日志
            sysLoginService.logout(username);
        }
        return R.ok();
    }

    /**
     * 刷新令牌
     * @param request
     * @return
     */
    @PostMapping("refresh")
    public R<?> refresh(HttpServletRequest request){
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (StringUtils.isNotNull(loginUser)){
            // 刷新令牌
            tokenService.refreshToken(loginUser);
            return R.ok();
        }
        return R.ok();
    }

    /**
     * 注册接口
     * @param registerBody
     * @return
     */
    @PostMapping("/register")
    public  R<?> register(@RequestBody RegisterBody registerBody){
        // 用户注册
        sysLoginService.register(registerBody.getUsername(),registerBody.getPassword());
        return R.ok();
    }
}
