package com.ruoyi.auth.service;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.core.constant.CacheConstants;
import com.ruoyi.core.constant.Constants;
import com.ruoyi.core.exception.ServiceException;
import com.ruoyi.redis.service.RedisService;
import com.ruoyi.system.api.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author wxh
 * @date 2024/5/14 0014
 */
@Component
public class SysPasswordService {

    private int maxRetryCount = CacheConstants.PASSWORD_MAX_RETRY_COUNT;

    private Long lockTime = CacheConstants.PASSWORD_LOCK_TIME;

    @Autowired
    private RedisService redisService;

    @Autowired
    private SysRecordLogService sysRecordLogService;

    /**
     * 缓存用户名，密码登录错误次数键名
     * @param username 用户名
     * @return 结果
     */
    public String getCacheKey(String username){
        return CacheConstants.PWD_ERR_CNT_KEY + username;
    }



    /**
     * 校验密码
     * @param user
     * @param password 密码
     */
    public void validate(SysUser user, String password){
        String username = user.getUserName();
        Integer retryCount = redisService.getCacheObject(getCacheKey(username));
        if (retryCount == null){
            retryCount = 0;
        }
        // 密码输入错误次数大于限定次数
        if (retryCount>= Integer.valueOf(maxRetryCount)){
            String errMsg = String.format("密码输入错误%s次，账号锁定%s分钟",retryCount,lockTime);
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL,errMsg);
            throw new ServiceException(errMsg);
        }
        // 校验密码是否输入正确
        if (!matches(user,password)){
            retryCount = retryCount + 1;
            sysRecordLogService.recordLogininfor(username,Constants.LOGIN_FAIL,String.format("输入密码错误%s次",retryCount));
            redisService.setCacheObject(getCacheKey(username),retryCount,lockTime, TimeUnit.MINUTES);
            throw new ServiceException("用户名不存在/密码错误");
        }else {
            // 登录成功，清楚密码错误次数的缓存
            clearLoginRecordCache(username);
        }
    }


    /**
     * 校验密码
     * @param user
     * @param rawPassword
     * @return
     */
    public boolean matches(SysUser user,String rawPassword){
        return SecurityUtils.matchesPassword(rawPassword, user.getPassword());
    }

    /**
     * 清楚密码错误次数的缓存
     * @param username
     */
    public void clearLoginRecordCache(String username){
        if (redisService.hasKey(getCacheKey(username))){
            redisService.deleteObject(getCacheKey(username));
        }
    }
}
