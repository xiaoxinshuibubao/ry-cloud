package com.ruoyi.auth.service;

import com.ruoyi.core.constant.Constants;
import com.ruoyi.core.constant.SecurityConstants;
import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.core.utils.ip.IpUtils;
import com.ruoyi.core.utils.uuid.IdUtils;
import com.ruoyi.system.api.RemoteLogService;
import com.ruoyi.system.api.domain.SysLogininfor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 日志记录方法
 * @author wxh
 * @date 2024/5/13 0013
 */
@Component
public class SysRecordLogService {

    @Autowired
    private RemoteLogService remoteLogServic;

    /**
     * 记录登录信息
     * @param username
     * @param status
     * @param message
     */
    public void recordLogininfor(String username,String status, String message){
        SysLogininfor sysLogininfor = new SysLogininfor();
        sysLogininfor.setUserName(username);
        sysLogininfor.setIpaddr(IpUtils.getIpAddr());
        sysLogininfor.setMsg(message);
        // 日志状态
        if (StringUtils.equalsAny(status, Constants.LOGIN_SUCCESS,Constants.REGISTER,Constants.LOGOUT)){
            sysLogininfor.setStatus(Constants.LOGIN_SUCCESS_STATUS);
        }else if (Constants.LOGIN_FAIL.equals(status)){
            sysLogininfor.setStatus(Constants.LOGIN_FAIL_STATUS);
        }
        remoteLogServic.saveLogininfor(sysLogininfor, SecurityConstants.INNER);
    }

}
