package com.ruoyi.auth.service;


;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.core.constant.CacheConstants;
import com.ruoyi.core.constant.Constants;
import com.ruoyi.core.constant.SecurityConstants;
import com.ruoyi.core.constant.UserConstants;
import com.ruoyi.core.domain.R;
import com.ruoyi.core.enums.UserStatus;
import com.ruoyi.core.exception.ServiceException;
import com.ruoyi.core.text.Convert;
import com.ruoyi.core.utils.StringUtils;
import com.ruoyi.core.utils.ip.IpUtils;
import com.ruoyi.redis.service.RedisService;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 登录校验方法
 *
 * @author wxh
 * @date 2024/5/13 0013
 */
@Component
public class SysLoginService {

    @Autowired
    private SysRecordLogService sysRecordLogService;

    @Autowired
    private RemoteUserService remoteUserService;

    @Autowired
    private SysPasswordService sysPasswordService;

    @Autowired
    private RedisService redisService;

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    public LoginUser login(String username, String password) {
        // 用户名或密码为空 错误
        if (StringUtils.isAnyBlank(username, password)) {
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, "用户名/密码必须填写");
            throw new ServiceException("用户名/密码必须填写");
        }
        // 密码长度不匹配 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH || password.length() > UserConstants.PASSWORD_MAX_LENGTH) {
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, "用户密码不在指定范围内");
            throw new ServiceException("用户密码不在指定范围内");
        }
        // 用户名长度不匹配 错误
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH || username.length() > UserConstants.USERNAME_MAX_LENGTH) {
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, "用户名不在指定范围内");
            throw new ServiceException("用户名不在指定范围内");
        }
        // IP黑名单校验
        String blackStr = Convert.toStr(redisService.getCacheObject(CacheConstants.SYS_LOGIN_BLACKIPLIST));
        if (IpUtils.isMatchedIp(blackStr, IpUtils.getIpAddr())) {
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, "很遗憾，访问IP已被列入系统黑名单");
            throw new ServiceException("很遗憾，访问IP已被列入系统黑名单");
        }
        // 查询用户信息
        R<LoginUser> userInfo = remoteUserService.getUserInfo(username, SecurityConstants.INNER);
        if (StringUtils.isNull(userInfo) || StringUtils.isNull(userInfo.getData())) {
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, "登录用户不存在");
            throw new ServiceException("登录用户:" + username + "不存在");
        }
        if (R.FAIL == userInfo.getCode()) {
            throw new ServiceException(userInfo.getMessage());
        }
        LoginUser loginUser = userInfo.getData();
        SysUser sysUser = userInfo.getData().getSysUser();
        if (UserStatus.DELETED.getCode().equals(sysUser.getStatus())) {
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, "对不起，您的账号已被删除，您的账号已被删除");
            throw new ServiceException("对不起，您的账号:" + username + "已被删除");
        }
        if (UserStatus.DISABLE.getCode().equals(sysUser.getStatus())) {
            sysRecordLogService.recordLogininfor(username, Constants.LOGIN_FAIL, "对不起，您的账号已被停用");
            throw new ServiceException("对不起，您的账号:" + username + "已被停用");
        }
        sysPasswordService.validate(sysUser, password);
        sysRecordLogService.recordLogininfor(username, Constants.LOGIN_SUCCESS, "登录成功");
        return loginUser;
    }

    public void logout(String username) {
        sysRecordLogService.recordLogininfor(username, Constants.LOGOUT, "退出成功");
    }

    public void register(String username, String password) {
        if (StringUtils.isAnyBlank(username, password)) {
            throw new ServiceException("用户名/密码必须填写");
        }
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH || username.length() > UserConstants.USERNAME_MAX_LENGTH) {
            throw new ServiceException("账户长度必须在2到20个字符之间");
        }
        if (username.length() < UserConstants.PASSWORD_MIN_LENGTH || username.length() > UserConstants.PASSWORD_MAX_LENGTH) {
            throw new ServiceException("密码长度必须在5到20个字符之间");
        }

        // 注册用户信息
        SysUser sysUser = new SysUser();
        sysUser.setUserName(username);
        sysUser.setPassword(SecurityUtils.encryptPassword(password));
        sysUser.setNickName(username);
        R<Boolean> registerResult = remoteUserService.registerUserInfo(sysUser, SecurityConstants.INNER);

        if (R.FAIL == registerResult.getCode()){
            throw new ServiceException(registerResult.getMessage());
        }
        // 记录日志
        sysRecordLogService.recordLogininfor(username,Constants.REGISTER,"注册成功");
    }
}
