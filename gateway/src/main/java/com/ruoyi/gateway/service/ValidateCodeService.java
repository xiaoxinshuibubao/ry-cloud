package com.ruoyi.gateway.service;

import com.ruoyi.core.exception.CaptchaException;
import com.ruoyi.core.web.domain.AjaxResult;

import java.io.IOException;

/**
 * 验证码处理
 * @author wxh
 * @date 2024/5/9 0009
 */
public interface ValidateCodeService {

    /**
     * 生成验证码
     * @return
     * @throws IOException
     * @throws CaptchaException
     */
    public AjaxResult createCaptcha() throws IOException, CaptchaException;

    /**
     * 校验验证码
     * @param key
     * @param value
     * @throws CaptchaException
     */
    public void checkCaptcha(String key, String value) throws  CaptchaException;

}
