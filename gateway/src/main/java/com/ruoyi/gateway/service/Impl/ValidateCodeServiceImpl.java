package com.ruoyi.gateway.service.Impl;

import com.google.code.kaptcha.Producer;
import com.ruoyi.core.constant.CacheConstants;
import com.ruoyi.core.constant.Constants;
import com.ruoyi.core.exception.CaptchaException;
import com.ruoyi.core.utils.uuid.IdUtils;
import com.ruoyi.core.web.domain.AjaxResult;
import com.ruoyi.gateway.config.properties.CaptchaProperties;
import com.ruoyi.gateway.service.ValidateCodeService;
import com.ruoyi.redis.service.RedisService;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FastByteArrayOutputStream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author wxh
 * @date 2024/5/9 0009
 */
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {

    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisService redisService;

    @Autowired
    private CaptchaProperties captchaProperties;

    /**
     * 生成验证码
     * @return
     * @throws IOException
     * @throws CaptchaException
     */
    @Override
    public AjaxResult createCaptcha() throws IOException, CaptchaException {
        AjaxResult ajax = AjaxResult.success();
        boolean captchaEnabled = captchaProperties.getEnabled();
        ajax.put("captchaEnabled",captchaEnabled);
        // 判断开关是否打开
        if (!captchaEnabled){
            return  ajax;
        }
        // 报存验证码信息
        String  uuid = IdUtils.simpleUUID();
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        //获取验证码类型
        String captchaType = captchaProperties.getType();
        // 生成验证码
        if ("math".equals(captchaType)){
            // 数字计算
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@")+1);
            image= captchaProducerMath.createImage(capStr);
        } else if ("char".equals(captchaType)) {
            // 字符串输出
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }

        redisService.setCacheObject(verifyKey,code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);

        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try {
            ImageIO.write(image,"jpg",os);
        }catch (IOException e){
            return AjaxResult.error(e.getMessage());
        }
        ajax.put("uuid",uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ajax;
    }

    @Override
    public void checkCaptcha(String key, String value) throws CaptchaException {

    }
}
