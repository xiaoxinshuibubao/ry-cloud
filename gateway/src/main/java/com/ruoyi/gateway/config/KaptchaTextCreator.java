package com.ruoyi.gateway.config;

import com.google.code.kaptcha.text.impl.DefaultTextCreator;

import java.util.Random;

/**
 *
 * @author wxh
 * @date 2024/5/9 0009
 */
public class KaptchaTextCreator extends DefaultTextCreator {
    private  static  final  String[] CNUMBERS = "0,1,2,3,4,5,6,7,8,9,10".split(",");

    @Override
    public String getText() {
        int result = 0;
        Random random = new Random();
        int x = random.nextInt(10);
        int y = random.nextInt(10);
        StringBuilder suChinese = new StringBuilder(); //验证码内容
        int randomperands = random.nextInt(3);
        if (randomperands == 0){ //乘法
            result  = x * y;
            suChinese.append(CNUMBERS[x]);
            suChinese.append("*");
            suChinese.append(CNUMBERS[y]);
        }else if (randomperands == 1){
            if ((x != 0) && y % x ==0){
                result = y / x;
                suChinese.append(CNUMBERS[y]);
                suChinese.append("");
                suChinese.append(CNUMBERS[x]);
            }else {
                result = x + y;
                suChinese.append(CNUMBERS[x]);
                suChinese.append("+");
                suChinese.append(CNUMBERS[y]);
            }
        }else if (randomperands == 2){
            if (x >= y){
                result = x - y;
                suChinese.append(CNUMBERS[x]);
                suChinese.append("-");
                suChinese.append(CNUMBERS[y]);
            }else {
                result = y - x;
                suChinese.append(CNUMBERS[y]);
                suChinese.append("+");
                suChinese.append(CNUMBERS[x]);
            }
        }else {
            result = x + y;
            suChinese.append(CNUMBERS[x]);
            suChinese.append("+");
            suChinese.append(CNUMBERS[y]);

        }
        suChinese.append("=?@"+result);
        return suChinese.toString();
    }
}
