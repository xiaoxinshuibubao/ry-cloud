package com.ruoyi.gateway.handler;

import com.ruoyi.core.exception.CaptchaException;
import com.ruoyi.gateway.service.ValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import com.ruoyi.core.web.domain.AjaxResult;

import java.io.IOException;


/**
 * 验证码获取
 * @author wxh
 * @date 2024/5/8 0008
 */
@Component
public class ValidateCodeHandler implements HandlerFunction<ServerResponse> {

    @Autowired
    private ValidateCodeService validateCodeService;

    @Override
    public Mono<ServerResponse> handle(ServerRequest request) {
        AjaxResult ajax;
        try {
            ajax = validateCodeService.createCaptcha();
        }catch (CaptchaException | IOException e){
            return Mono.error(e);
        }
        // BodyInserters.fromValue(ajax) 将ajax对象转换为json格式的响应体
        return ServerResponse.status(HttpStatus.OK).body(BodyInserters.fromValue(ajax));
    }
}
