package com.ruoyi.system.api.util;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.ruoyi.core.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author wxh
 * @date 2024/5/11 0011
 */
public class CodeGeneratorUtil {
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入："+tip +":");
        System.out.println(help.toString());
        if (scanner.hasNext()){
            String ipt = scanner.next();
            if (StringUtils.isNoneBlank(ipt)){
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的"+tip+"!");
    }

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

//        File file = new File("api/api-system");
//        String path = file.getAbsolutePath();
        //全局配置
        GlobalConfig gc = new GlobalConfig();
//        String projectPath = System.getProperty("user.dir");
//        System.out.println("文件跟路径:--------"+projectPath);
//        File outputDir = new File(projectPath,"api/api-system/src/main/java");
//        System.out.println("设置代码生成路径:--------"+outputDir.getPath());
        File file = new File("api/api-system");
        String path = file.getAbsolutePath();
        System.out.println(path);
        gc.setOutputDir(path + "/src/main/java");
        gc.setFileOverride(true); //是否覆盖以前的文件
        gc.setOpen(false); // 是否打开生成目录
        gc.setAuthor("wxx"); // 设置作者名称
        gc.setIdType(IdType.AUTO); //设置主键策略
        gc.setBaseResultMap(true); //生成基本ResultMap
        gc.setBaseColumnList(true); //生成基本ColumnList
        gc.setServiceName("%Service"); // 去掉服务默认前缀
        gc.setDateType(DateType.ONLY_DATE); //设置时间类型
        mpg.setGlobalConfig(gc);
        
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/ry-cloud?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=UTC");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("root");
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.ruoyi.system.api"); // 生成代码所在的根路径
        pc.setMapper("mapper"); // mapper接口文件路径
        pc.setXml("mapper.xml"); // xml映射文件路径
        pc.setEntity("domain"); // 实体类路径
        pc.setService("service"); // service路径
        pc.setServiceImpl("service.impl"); // service实现类路径
        pc.setController("controller");
        mpg.setPackageInfo(pc);

        // 策略配置
        StrategyConfig sc = new StrategyConfig();
        // 数据库表名到实体类的命名策略,NamingStrategy.underline_to_camel表示采用下划线转驼峰的命名方式
        sc.setNaming(NamingStrategy.underline_to_camel);
        // 数据库字段到属性的命名策略,NamingStrategy.underline_to_camel表示采用下划线转驼峰的命名方式
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        sc.setEntityLombokModel(true); // 自动lombok
//        sc.setRestControllerStyle(true); // 是否采用RESTful风格
//        sc.setControllerMappingHyphenStyle(true); // url是否开启驼峰转连字符
        sc.setLogicDeleteFieldName("deleted"); //设置逻辑删除

        // 设置自动填充配置
        TableFill createTime = new TableFill("create_time", FieldFill.INSERT);
        TableFill updateTime = new TableFill("update_time", FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFills = new ArrayList<TableFill>();
        tableFills.add(createTime);
        tableFills.add(updateTime);
        sc.setTableFillList(tableFills);

        // 乐观锁
//        sc.setVersionFieldName("version");
        sc.setRestControllerStyle(true); //驼峰命名

        sc.setInclude(scanner("表名，多个英文逗号分割").split(","));
        mpg.setStrategy(sc);

        mpg.execute();
    }

}
