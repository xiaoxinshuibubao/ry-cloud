package com.ruoyi.system.api.factory;

import com.ruoyi.core.domain.R;
import com.ruoyi.system.api.RemoteLogService;
import com.ruoyi.system.api.domain.SysLogininfor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 日志服务降级处理
 * @author wxh
 * @date 2024/5/13 0013
 */
@Component
@Slf4j
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteLogService> {


    @Override
    public RemoteLogService create(Throwable throwable) {
        log.error("日志服务调用失败:{}",throwable.getMessage());
        return new RemoteLogService() {
            @Override
            public R<Boolean> saveLogininfor(SysLogininfor sysLogininfor, String source) {
                return null;
            }
        };
    }
}
