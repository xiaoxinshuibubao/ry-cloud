package com.ruoyi.system.api;

import com.ruoyi.core.constant.SecurityConstants;
import com.ruoyi.core.constant.ServiceNameConstant;
import com.ruoyi.core.domain.R;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 用户服务
 *
 * @author wxh
 * @date 2024/5/13 0013
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstant.SYSTEM_SERVICE)
public interface RemoteUserService {

    /**
     * 通过用户名获取用户信息
     *
     * @param username 用户名
     * @param source   请求来源
     * @return 结果
     */
    @GetMapping("/user/info/{username}")
    R<LoginUser> getUserInfo(@PathVariable("username") String username, @RequestHeader(SecurityConstants.INNER) String source);

    /**
     * 注册用户信息
     * @param sysUser
     * @param source
     * @return
     */
    @PostMapping("/user/register")
    R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.INNER) String source);
}
