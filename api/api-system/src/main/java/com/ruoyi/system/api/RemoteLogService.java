package com.ruoyi.system.api;

import com.ruoyi.core.constant.SecurityConstants;
import com.ruoyi.core.constant.ServiceNameConstant;
import com.ruoyi.core.domain.R;
import com.ruoyi.system.api.domain.SysLogininfor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * 日志服务
 * @author wxh
 * @date 2024/5/13 0013
 */
@FeignClient(contextId = "remoteLogService",value = ServiceNameConstant.SYSTEM_SERVICE)
public interface RemoteLogService {


    /**
     * 保存访问记录
     * @param sysLogininfor 访问实体
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("logininfor")
    public R<Boolean> saveLogininfor(@RequestBody SysLogininfor sysLogininfor, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
